﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringInverter
{
    internal class CalcOp
    {
        private Operand _op1, _op2;
        public CalcOp(Operand val1, Operand val2)
        {
            this._op1 = val1;
            this._op2 = val2;
        }





        public Operand Add()
        {   return new Operand(_op1.Val + _op2.Val);   }




        public Operand Mult()
        {   return new Operand(_op1.Val * _op2.Val);    }
    



        public Operand Sub()
        {   return new Operand(_op1.Val - _op2.Val);    }



        public Operand Div()
        {
            try
            {
                if (_op2.Val != 0)
                    return new Operand(_op1.Val / _op2.Val);

                else
                    throw new DivideByZeroException();
            }
            catch(Exception e)
            {
                Console.WriteLine($"Ops !! Can not divide by 0\n {e.Message}");
                return new Operand (0);

            }
        
        }
    }
    
}