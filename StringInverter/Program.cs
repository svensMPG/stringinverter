using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;


namespace StringInverter
{
    class Program
    {
        static void Main(string[] args)
        {
            //Strrev(); // appel d une fonction/ methode dans le main FR
            //StartCalc(); // calcul a travers des nombres donnees FR
            StartCalcOp(); // calcul des nombres donnees a travers des classes: creation d un objet [objet est du type classe] FR
            Console.WriteLine("    \nSahar Gabtni!!!!!! \n");
        }

        static void Strrev()
        {
            Console.WriteLine("     Strrev      !!");
            StrInv str;
            string s = "12345";
            str = new StrInv(s);
            str = new StrInv();
            //str.InvertString(); :: appel d une fonction/ methode dans une autre classe FR
        }


        static void StartCalc()
        {
            float a = 24;
            float b = 24;
            Console.WriteLine("     StartCalc      !!"); // retour a la ligne FR

            Calculatrice cal = new Calculatrice(a, b); ; //cal is an object type Calculatrice and the Calculatrice constructor takes  2 float as parameters
            
            cal.Add(); // hier rufe ich die Methoed + auf to add  a and b
            cal.Div(); // hier rufe ich die Methoed / auf to div a by b
            cal.Sub(); // hier rufe ich die Methoed - auf to sub a from b
            cal.Mult(); // hier rufe ich die Methoed * auf to mult a by b
        }

        static void StartCalcOp()
        {
           
            Console.WriteLine("     StartCalOp      !!"); // retour a la ligne FR

            Operand a = new Operand(12); // a is an object type Operand and the Operand constructor takes float as a parameter
            Operand b = new Operand(0); // b is an object type Operand and the Operand constructor takes float as a parameter

            //CalcOp calcOp = new CalcOp(a, b); // calcOp is an object type CalcOp and the CalcOp constructor takes 2 parameters type Operand and in this case are a and b
            // so I have the access to the 4 methods => Add, Sub, Mult and Div


            var c = a + b; // a, b and c are objects type Operand and c here takes the result of the add of a and b and this is related to the operator in the classe Operand
            // and through the test I realized that the oeprator replaces the functions => Add, Sub, Mult and Div;;;;;; through this operator we have the 4 Methos abandon bc the operation is created inside of the operator 
            c = a * b; //  c  takes the result of the mult of a by b
            c = a - b; // c here takes the result of the sub of a from b
            c = a / b;//  c here takes the result of the div of a by b



           // c = calcOp.Add(); // the Method Add is type Operand and returns the add of 2 Operand-objects => in the class Operand we've created the method Val that has the getter and the setter
            // where in this case the getter is c the addition of 8 and 0 the setter is 8 / 0 

           // c = calcOp.Div(); // the Method Div is type Operand and returns the add of 2 Operand-objects => in the class Operand we've created the method Val that has the getter and the setter
            // where in this case the getter is the div of 8 by 0 the setter is 8 / 0

            // c = calcOp.Mult(); // the Method Mult is type Operand and returns the add of 2 Operand-objects => in the class Operand we've created the method Val that has the getter and the setter
            // where in this case the getter is the mult of 8 by 0 the setter is 8 / 0

            // c = calcOp.Sub(); // the Method Sub is type Operand and returns the add of 2 Operand-objects => in the class Operand we've created the method Val that has the getter and the setter
            // where in this case the getter is the substraction of 8 from 0 the setter is 8 / 0

        }
    }
}
