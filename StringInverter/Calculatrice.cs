﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringInverter
{
    internal class Calculatrice
    {

        private float x;
        private float y;

      

        public void Add()
        {
            Console.WriteLine(" Adding x to y ...");
            Console.WriteLine($"The result of the add : {x + y}\n");
        }



        public void Mult()
        {
            Console.WriteLine(" Multiplying x to y ...");
            Console.WriteLine($"The result of the multiplication : {x * y} \n");
        }




        public void Sub()
        {
            Console.WriteLine(" Substracting x to y ...");
            Console.WriteLine($"The result of the substraction is : {x - y}\n");
        }




        public void Div()
        {
            Console.WriteLine(" Dividing x to y ...");
            //while (y != 0)
            //{
            //    Console.WriteLine($"The result of the division is : {x / y}");
            //}
            try 
            {
                float result = float.NegativeInfinity;
                if (y != 0)
                {
                    
                    result = x / y;
                    Console.WriteLine($"The result of the division is : {result}\n");
                }
                else
                    throw new DivideByZeroException();
            }
            catch (Exception err)
            {
                
                
                    Console.WriteLine($"The result of the division is : infinity\n");
                    Console.WriteLine($"Ops !! Can not divide by 0\n {err.Message}");
                
       

            }
        }

        public Calculatrice(float x, float y)
        {
            this.x = x;
            this.y = y;

            //Add();
            //Mult();
            //Div();
            //Sub();
        }
    }
}

