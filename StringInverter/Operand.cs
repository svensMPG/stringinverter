﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringInverter
{
    internal class Operand
    {
        private float _val;
        public Operand(float a)
        {   this.Val = a;   }


        public float Val { get => _val; private set => _val = value; } // the get-Accessor: the result;; set-Accessor: is only set upon the inizialisation :: the value we gave 8/0 


        public static Operand operator +(Operand a, Operand b) => new Operand(a.Val + b.Val); 
        public static Operand operator -(Operand a, Operand b) => new Operand(a.Val - b.Val);
        public static Operand operator *(Operand a, Operand b) => new Operand(a.Val * b.Val);
        public static Operand operator /(Operand a, Operand b)
        {
            try
            {
                if (b.Val != 0)
                    return new Operand(a.Val / b.Val);
                else
                    throw new DivideByZeroException();
            }
            catch (Exception e) 
            {
                Console.WriteLine($"Ops !! Can not divide by 0\n {e.Message}");
                return new Operand(0);
            }
        }
    }
}
