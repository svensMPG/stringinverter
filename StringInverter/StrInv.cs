﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringInverter
{
    internal class StrInv
    {
        public string str = "Sahar Sven und Habib";
#pragma warning disable CS8618 // Ein Non-Nullable-Feld muss beim Beenden des Konstruktors einen Wert ungleich NULL enthalten. Erwägen Sie die Deklaration als Nullable.
        public StrInv()
#pragma warning restore CS8618 // Ein Non-Nullable-Feld muss beim Beenden des Konstruktors einen Wert ungleich NULL enthalten. Erwägen Sie die Deklaration als Nullable.
        {
            int j = 0;
            foreach (char esp in str)
            {
                if (esp == ' ')
                    j++;
            }
            Console.WriteLine($"le nombre d espace est : {j}");
        }


        public void InvertString()
        { 
        }
        public StrInv(string inputStrToReverse)
        {
            string revstr = "";
            for (int i = 0; i < inputStrToReverse.Length; i++)
            {
                revstr = inputStrToReverse[i] + revstr;
            }
            Console.WriteLine($"la chaine de caractere : {inputStrToReverse}");
            Console.WriteLine($"l inverse de la chaine de caractere : {revstr}");


            /*************************************************************************
            oder 
            char[] rev = inputStrToReverse.ToCharArray(); // converts a char to an array
            Array.Reverse(rev); // method that reverses an array
            string result = new string(rev);
            Console.WriteLine($"la chaine de caractere : {inputStrToReverse}");
            Console.WriteLine($"l inverse de la chaine de caractere : {result}");
            ***************************************************************************/
        }


    }
}
